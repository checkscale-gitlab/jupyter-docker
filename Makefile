.PHONY=image run shell

TAG=hzahnleiter/jupyter-docker:2.1.0
NOTEBOOK_DIR=$(shell pwd)/example_notebooks

image:
	docker build -t $(TAG) .

run:
	docker run --name jupyterdocker \
		-p 8888:8888 -p 2222:22 \
		--rm -ti \
		-v $(NOTEBOOK_DIR):/home/jovyan $(TAG)

shell:
	docker exec -it jupyterdocker bash
