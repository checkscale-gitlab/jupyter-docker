# Changelog

# 2.1.0

- I coudn't get BeakerX running correctly. It threw a lot of java compile
  errors. Hence, I reverted to IJava kernel
- Now using JupyterLab 3.0.5

# 2.0.0

-  Now using JupyterLab 3.0.5
-  BeakerX for JVM support

# 1.1.0

-  Now using Spencer Park's latest IJave kernel (1.3.0) and Java 11.

# 1.0.1

-  Small improvements, still Java 10

# 1.0.0 Initial version.

-  Jupyter Notebook 5.5, Python 3 and Java 10
